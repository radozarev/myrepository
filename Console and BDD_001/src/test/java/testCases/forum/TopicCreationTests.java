package testCases.forum;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import pages.forum.HomePage;

public class TopicCreationTests extends BaseTest {

    private final String EMAIL = "kravdit@gmail.com";
    private final String PASSWORD = "buddyGroup3";
    private final HomePage homePage = new HomePage(actions.getDriver());

    @After
    public void goToHomePage(){
        actions.clickElement("forum.homePage.logo");
    }

    @Test
    public void createNewTopic(){
        homePage.registeredUser(EMAIL, PASSWORD);

        actions.clickElement("forum.homePage.newTopicButton");
        actions.typeValueInField("Maecenas vulputate sit amet lectus", "forum.homePage.titleField");
        actions.typeValueInField("Donec massa felis, eleifend nec mollis vel, ultricies", "forum.homePage.descriptionField");
        actions.clickElement("forum.homePage.createTopicButton");

        Assert.assertTrue("New topic isn't posted", actions.isElementVisible("forum.newTopicPage.newTopicTitle"));
        Assert.assertTrue("Reply button isn't displayed", actions.isElementVisible("forum.newTopicPage.topicReplyButton"));
    }
}
