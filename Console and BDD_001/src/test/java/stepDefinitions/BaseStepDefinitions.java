package stepDefinitions;

import com.telerikacademy.forumframework.UserActions;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeStory;
import pages.forum.HomePage;
import pages.forum.LogInPage;
import pages.forum.NewTopicPage;

public class BaseStepDefinitions {

    UserActions actions = new UserActions();
    HomePage home = new HomePage(actions.getDriver());
    LogInPage logInPage = new LogInPage(actions.getDriver());
    NewTopicPage newTopicPage = new NewTopicPage(actions.getDriver());

    @BeforeStory
    public void setUp(){ UserActions.loadBrowser("forum.homePage"); }

    @AfterStory
    public static void tearDown(){
        UserActions.quitDriver();
    }
}
