package stepDefinitions;

import org.jbehave.core.annotations.*;
import org.junit.Assert;

public class StepDefinitions extends BaseStepDefinitions {

    private final String EMAIL = "kravdit@gmail.com";
    private final String PASSWORD = "buddyGroup3";

    @BeforeScenario
    public void authenticateRegisteredUser(){
        home.registeredUser(EMAIL, PASSWORD);
    }

    @AfterScenario
    public void goToHomePage(){
        actions.clickElement("forum.homePage.logo");
    }

    @Given("user is logged and the  topic creation button is visible")
    public void searchNewTopicButtonVisible(){ actions.waitForElementVisible("forum.homePage.newTopicButton"); }

    @When("button new topic  is clicked")
    public void clickNewTopicButton(){
        actions.clickElement("forum.homePage.newTopicButton");
    }

    @When("$title is typed in title field")
    public void fillTitleField(String title){
        actions.typeValueInField(title, "forum.homePage.titleField");
    }

    @When("$description is typed in description field")
    public void fillDescriptionField(String description){
        actions.typeValueInField(description, "forum.homePage.descriptionField");
    }

    @When("create topic button is clicked")
    public void clickCreateTopicButton(){
        actions.clickElement("forum.homePage.createTopicButton");
    }

    @Then("the new topic is created")
    public void assertNewTopicPosted(){
        Assert.assertTrue(actions.isElementVisible("forum.newTopicPage.newTopicTitle"));
    }

    @Then("reply to topic button is visible")
    public void assertReplyButtonVisible(){
        Assert.assertTrue(actions.isElementVisible("forum.newTopicPage.topicReplyButton"));
    }

}