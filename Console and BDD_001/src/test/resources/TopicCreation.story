Meta:
@forumTopicCreation

Narrative:
As a registered user of forum
I would like to create a new topic
So that I can  post a new topic in the forum

Scenario: Create a new topic with valid title and description
Given user is logged and the  topic creation button is visible
When button new topic  is clicked
And Maecenas vulputate sit amet lectus is typed in title field
And Donec massa felis, eleifend nec mollis vel, ultricies is typed in description field
And create topic button is clicked
Then the new topic is created
And reply to topic button is visible