package pages.forum;

import com.telerikacademy.forumframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver, "forum.homePage");
    }

    public void registeredUser(String email, String password){
        if(actions.isElementPresent("forum.homePage.logInButton")){
            actions.clickElement("forum.homePage.logInButton");
            actions.assertNavigatedUrl("forum.logInPage");

            LogInPage logInPage = new LogInPage(actions.getDriver());
            logInPage.fillInCredentials(email, password);
        }
    }

}
