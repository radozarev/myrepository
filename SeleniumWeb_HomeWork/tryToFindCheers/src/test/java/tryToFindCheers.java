import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.pagefactory.ByAll;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class tryToFindCheers {

    @Test
    public void navigateTo(){
        /*System.setProperty("webdriver.chrome.driver", "C:\\selenium\\selenium-java-3.141.59\\chromedriver_win32\\chromedriver.exe");
        WebDriver webdriver = new ChromeDriver();*/

        System.setProperty("webdriver.edge.driver", "C:\\selenium\\selenium-java-3.141.59\\edgedriver_win64\\msedgedriver.exe");
        WebDriver webdriver = new EdgeDriver();
        WebDriverWait wait = new WebDriverWait(webdriver, 10);
        /*System.setProperty("webdriver.gecko.driver", "C:\\selenium\\selenium-java-3.141.59\\geckodriver-v0.29.1-win64\\geckodriver.exe\"");
        File pathToBinary = new File("\"C:\\Program Files\\Mozilla Firefox\\firefox.exe\"");*/

        //Arrange
        webdriver.get("https://stage-forum.telerikacademy.com");
        webdriver.manage().window().maximize();
        webdriver.manage().deleteAllCookies();
        webdriver.findElement(By.xpath("//span[@class='d-button-label']")).click();
        webdriver.findElement(By.xpath("//input[@id='Email']")).sendKeys("kravdit@gmail.com");
        webdriver.findElement(By.xpath("//input[@id='Password']")).sendKeys("buddyGroup3");
        webdriver.findElement(By.xpath("//button[normalize-space()='Sign in']")).submit();

        //Act
        WebElement searchButton = webdriver.findElement(By.xpath("//a[@id='search-button']"));
        searchButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='search topics, posts, users, or categories']")));
        WebElement elem = webdriver.findElement(By.xpath("//input[@placeholder='search topics, posts, users, or categories']"));
        elem.sendKeys("Cheers!");
        elem.sendKeys(Keys.ENTER);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='search-link']//span[2]")));
        WebElement element = webdriver.findElement(By.xpath("//a[@class='search-link']//span[2]"));
        element.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Cheers!Cheers!")));
        String cheers = webdriver.findElement(By.linkText("Cheers!Cheers!")).getText();

        //Assert
        Assert.assertEquals("Cheers!Cheers!", cheers);

        webdriver.close();
    }
}
