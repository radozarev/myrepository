import Pages.ForumHomePage;
import Pages.ForumLogInPage;
import Pages.NewTopicCreation;
import Pages.SearchPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TryToFindCheers {

    public static final String TYTLE_FIELD = "The standard chunk of Lorem Ipsum";
    public static final String TEXT_FIELD = "This book is a treatise on the theory of ethics";

    private WebDriver webdriver;
    private WebDriverWait wait;

    @Before
    public void navigateTo(){
        System.setProperty("webdriver.chrome.driver", "C:\\selenium\\selenium-java-3.141.59\\chromedriver_win32\\chromedriver.exe");
        webdriver = new ChromeDriver();
        wait = new WebDriverWait(webdriver, 10);
        webdriver.get("http://stage-forum.telerikacademy.com/");
    }

    @After
    public void closeBrowser() {
        webdriver.close();
        webdriver.quit();
    }
    @Test
    public void creataTopic(){
        ForumHomePage homePage = new ForumHomePage(webdriver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(.,'Log In')]")));
        homePage.getLogInButton().click();

        ForumLogInPage logInPage = new ForumLogInPage(webdriver);
        logInPage.signIn();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("create-topic")));
        homePage.createNewTopic(TYTLE_FIELD, TEXT_FIELD);
        NewTopicCreation newTopicPage = new NewTopicCreation(webdriver);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@title='begin composing a reply to this topic']")));

        Assert.assertEquals(TYTLE_FIELD, newTopicPage.getTextName().getText());
    }


    @Test
    public void findCheers(){

        ForumHomePage homePage = new ForumHomePage(webdriver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(.,'Log In')]")));
        homePage.getLogInButton().click();

        ForumLogInPage logInPage = new ForumLogInPage(webdriver);
        logInPage.signIn();

        SearchPage searchPage = new SearchPage(webdriver);
        searchPage.getFindButton().click();
        searchPage.getSearchButton();
        searchPage.typeInSearch();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='search-link']//span[2]")));
        searchPage.getCheers();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Cheers!Cheers!")));

        String cheers = searchPage.getStringCheers().getText();

        Assert.assertEquals("Cheers!Cheers!", cheers);

    }

}
