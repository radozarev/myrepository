package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForumLogInPage {

    private WebDriver webdriver;

    public ForumLogInPage(WebDriver webdriver){
        this.webdriver = webdriver;
    }

    public WebElement getEmailInput(){
        return webdriver.findElement(By.xpath("//input[@id='Email']"));
    }

    public WebElement getPasswordInput(){
        return webdriver.findElement(By.xpath("//input[@id='Password']"));
    }

    public WebElement getSignInButton(){
        return webdriver.findElement(By.xpath("//button[normalize-space()='Sign in']"));
    }

    public void signIn(){
        getEmailInput().sendKeys("kravdit@gmail.com");
        getPasswordInput().sendKeys("buddyGroup3");
        getSignInButton().click();
    }
}
