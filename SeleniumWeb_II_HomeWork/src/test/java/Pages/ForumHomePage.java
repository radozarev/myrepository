package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ForumHomePage {

    private WebDriver webdriver;

    public ForumHomePage(WebDriver webdriver){
        this.webdriver = webdriver;
    }

    public WebElement getLogInButton(){
        return webdriver.findElement(By.xpath("//span[@class='d-button-label']"));
    }

    public WebElement getNewTopicButton() {
        return webdriver.findElement(By.id("create-topic"));
    }

    public WebElement getTitleField() {
        return webdriver.findElement(By.id("reply-title"));
    }

    public WebElement getDescriptionField() {
        return webdriver.findElement(By.xpath("//textarea[contains(@aria-label,'Type here')]"));
    }

    public WebElement getCreateTopicButton() {
        return webdriver.findElement(By.xpath("//button[contains(.,'Create Topic')]"));
    }


    public void createNewTopic(String title, String description){
        WebDriverWait wait = new WebDriverWait(webdriver, 10);
        getNewTopicButton().click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@role='form']")));
        getTitleField().sendKeys(title);
        getDescriptionField().sendKeys(description);
        getCreateTopicButton().click();
    }

}
