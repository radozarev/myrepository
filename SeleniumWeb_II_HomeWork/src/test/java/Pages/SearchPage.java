package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchPage {

    private WebDriver webdriver;

    public SearchPage(WebDriver webdriver){
        this.webdriver = webdriver;
    }

    public WebElement getFindButton(){
        return webdriver.findElement(By.xpath("//a[@id='search-button']"));
    }

    public WebElement getSearchButton(){
        return webdriver.findElement(By.xpath("//input[@placeholder='search topics, posts, users, or categories']"));
    }

    public WebElement getCheersElement(){
        return webdriver.findElement(By.xpath("//a[@class='search-link']//span[2]"));
    }

    public WebElement getStringCheers(){
        return webdriver.findElement(By.linkText("Cheers!Cheers!"));
    }

    public void typeInSearch(){
        getFindButton().click();
        getSearchButton().sendKeys("Cheers!");
        getSearchButton().sendKeys(Keys.ENTER);
    }

    public void getCheers(){
        getCheersElement().click();
    }

}