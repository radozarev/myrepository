package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NewTopicCreation {

    private WebDriver webdriver;

    public NewTopicCreation(WebDriver webdriver){
        this.webdriver = webdriver;
    }

    public WebElement getTextName(){
        return webdriver.findElement(By.xpath("//div[@class='title-wrapper']//a[1]"));
    }
}
