package pages.trello;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import trelloapi.Models.CardModel;

public class BoardPage extends BaseTrelloPage {

    public BoardPage(WebDriver driver) {
        super(driver, "trello.boardUrl");
    }

    public void addCardToList(String cardName, String listName){
        actions.waitForElementVisible("trello.boardPage.addCardInListButton", listName);
        actions.clickElement("trello.boardPage.addCardInListButton", listName);

        actions.waitForElementVisible("trello.boardPage.cardTitleField");
        actions.typeValueInField(cardName, "trello.boardPage.cardTitleField");
        actions.clickElement("trello.boardPage.publishCardInListButton");
    }

    public void moveCardToList(CardModel createdCard, String listName){
        String elementLocator = String.format(Utils.getUIMappingByKey("trello.boardPage.cardByName"), createdCard.name);
        String targetLocator = String.format(Utils.getUIMappingByKey("trello.boardPage.listByName"), listName);

        actions.waitForElementVisible("trello.boardPage.cardByName", createdCard.name);
        WebElement element = driver.findElement(By.xpath(elementLocator));
        WebElement target = driver.findElement(By.xpath(targetLocator));

        Actions action = new Actions(driver);
        action.dragAndDrop(element, target).build().perform();
    }

    public void deleteBoard(){
        actions.waitForElementVisible("trello.boardPage.showMenuButton");
        actions.clickElement("trello.boardPage.showMenuButton");

        actions.waitForElementVisible("trello.boardPage.showMenuMoreButton");
        actions.clickElement("trello.boardPage.showMenuMoreButton");

        actions.waitForElementVisible("trello.boardPage.closeBoardButton");
        actions.clickElement("trello.boardPage.closeBoardButton");

        actions.waitForElementVisible("trello.boardPage.confirmCloseBoardButton");
        actions.clickElement("trello.boardPage.confirmCloseBoardButton");

        actions.waitForElementVisible("trello.boardPage.deleteBoardButton");
        actions.clickElement("trello.boardPage.deleteBoardButton");

        actions.waitForElementVisible("trello.boardPage.confirmDeleteBoardButton");
        actions.clickElement("trello.boardPage.confirmDeleteBoardButton");
    }

    public void assertListExists(String listName){
        actions.waitForElementPresent("trello.boardPage.listByName", listName);
    }

    public void assertCardExists(String cardName){
        actions.waitForElementPresent("trello.boardPage.cardByName", cardName);
    }

    public void assertCardIsInList(String listName, String cardName){
        actions.waitForElementPresent("trello.boardPage.cardByNameInListByName", listName, cardName);
    }

    public void assertBoardCannotBeFound(){
        actions.refreshBrowser();
        actions.waitForElementPresent("trello.boardPage.boardNotFoundMessage");
    }
}
