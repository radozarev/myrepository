package trelloapi.Models;

import java.util.Date;
import java.util.List;

public class BoardModel {
    public String name;
    public String desc;
    public Object descData;
    public boolean closed;
    public Object idOrganization;
    public Object idEnterprise;
    public Object limits;
    public Object pinned;
    public String shortLink;
    public List<Object> powerUps;
    public Date dateLastActivity;
    public List<Object> idTags;
    public Object datePluginDisable;
    public Object creationMethod;
    public Object ixUpdate;
    public boolean enterpriseOwned;
    public Object idBoardSource;
    public Object idMemberCreator;
    public String id;
    public boolean starred;
    public String url;
    public boolean subscribed;
    public Date dateLastView;
    public String shortUrl;
    public Object templateGallery;
    public List<Object> premiumFeatures;
}
