# Problem: Generate test ideas based on the Test Design Techniques about forum topics. Focus on topic creation, commenting and notifications. Make sure to add priority and to  mention which technique you used for each test case.


|Test Case Title|Priority|Technique|
|---|---|---|
|Check whether the login works with correct credentials|Prio 1|Pairwise testing|
|Topic title should not be empty|Prio 1|Pairwise testing|
|Topic title and post field cannot be empty|Prio 1|Pairwise testing|
|Topic title must be b/n one and five characters|Prio 2|Boundary value analysis|
|Tag must have atleast one element|Prio 1|Exploratory testing​|
|Bold and Italic styles should work together|Prio 3| Pairwise testing|
|Minimum and Maximum lengths should be set for all the text boxes|Prio 3|Boundary value analysis|
|Check for possible combination b/n title, category, task |Prio 1|Decision tables|
|Check for behaiviour of different file format in text field|Prio 1|Pairwise testing|
|Check for different styles combination in comment text field|Prio 2|Decision tables|
|Can not posting an empty post|Prio 1|Use case testing|
|Minimum and Maximum lengths should be set for all the text boxes|Prio 1|Boundary value analysis|
|Check different levels for notification|Prio 1|Equivalence partitioning |
|Check on posting an empty comment|Prio 2|Exploratory testing|
|Check for min. and max. length in comment field|Prio 2|Boundary value analysis|
|Active comment can be edited|Prio 2|State transition testing|
|Deleted comment can not be edited|Prio 2|State transition testing|
|Active comment can be deleted|Prio 2|State transition testing|
|Deleted comment can be activated|Prio 2|State transition testing|
|User can create a new topic|Prio 1|Use case testing|
|User can edit only own topic|Prio 1|Use case testing|
|User can delete only own topic|Prio 1|Use case testing|
|User can create a new comment|Prio 1|Use case testing|
|User can edit own comment|Prio 1|Use case testing|
|User can delete comment|Prio 1|Use case testing|
|Administrator can create a new topic|Prio 1|Use case testing|
|Administratpr can edit each topic|Prio 1|Use case testing|
|Administrator can delete each topic|Prio 1|Use case testing|
|Administrator create a new comment|Prio 1|Use case testing|
|Administrator can delete each comment|Prio 1|Use case testing|
|Active comment can be shared in facebook|Prio 2|Use case testing|
|Active comment can be shared in tweeter|Prio 2|Use case testing|
|Active comment can be shared via email|Prio 2|Use case testing|
|Active comment can be bookmarked|Prio 2|Use case testing|
|Active comment can be flagged|Prio 2|Use case testing|
|User can select differnt type of notification|Prio 1|Use case testing|
|Check for interface specifications|Prio 1|Static analisys|
|Unregistered user can't access forum content|Prio 1|Use case testing|